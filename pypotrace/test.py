import py_trace

inp  = {}
inp['width'] = 200
inp['height'] = 200


pixels = [0]*inp['width']*inp['height']
radius = 50
for y in range(inp['height']):
    for x in range(inp['width']):
        x0 = int(x - inp['width']/2)
        y0 = int(y - inp['height']/2)
        v = 1 if x0*x0+y0*y0<radius*radius else 0
        pixels[x*inp['width']+y] = v

inp['pixels'] = pixels
               
out = py_trace.py_trace(inp)


import matplotlib.pyplot as plt
import numpy as np

points = np.array(out['points'],  dtype=float)
points = points.reshape(-1,2)
for i in range( out['n_segment'] ):
    current_points =  points[out['xadj'][i]:out['xadj'][i+1],:]
    x = np.concatenate( ( current_points[:,0], [current_points[0,0]] ) )
    y = np.concatenate( ( current_points[:,1], [current_points[0,1]] ) )
    plt.plot(x,y)

plt.show()
    
# print ( out )
