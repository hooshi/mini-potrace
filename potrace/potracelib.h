/* Copyright (C) 2001-2017 Peter Selinger.
   This file is part of Potrace. It is free software and it is covered
   by the GNU General Public License. See the file COPYING for details. */

#ifndef POTRACELIB_H
#define POTRACELIB_H

// this file defines the API for the core Potrace library. For a more
//   detailed description of the API, see potracelib.pdf 

// If we don't this Macro, Clang format will freak out!
#define EXTERN_C_BEGIN \
  extern "C"           \
  {
#define EXTERN_C_END }

#ifdef __cplusplus
EXTERN_C_BEGIN
#endif

// =========================================================================
//   Progress callback -- optional
// =========================================================================

struct potrace_progress_s
{
  void (*callback)(double progress, void * privdata); /* callback fn */
  void * data; /* callback function's private data */
  double min, max; /* desired range of progress, e.g. 0.0 to 1.0 */
  double epsilon; /* granularity: can skip smaller increments */
};
typedef struct potrace_progress_s potrace_progress_t;

// =========================================================================
//   Bitmap object
// =========================================================================

/* native word size */
typedef unsigned long potrace_word;

/* Internal bitmap format. The n-th scanline starts at scanline(n) =
   (map + n*dy). Raster data is stored as a sequence of potrace_words
   (NOT bytes). The leftmost bit of scanline n is the most significant
   bit of scanline(n)[0]. */
struct potrace_bitmap_s
{
  int w, h; /* width and height, in pixels */
  int dy; /* words per scanline (not bytes) */
  potrace_word * map; /* raw data, dy*h words */
};
typedef struct potrace_bitmap_s potrace_bitmap_t;

// Create a bitmap from integers
potrace_bitmap_t* potrace_bitmap_create(int w, int h, unsigned char *is_black); //TODO

// Free a bitmap
void potrace_bitmap_free(potrace_bitmap_t*); //TODO

// =========================================================================
//   Potrace parameters
// =========================================================================

/* turn policies */
#define POTRACE_TURNPOLICY_BLACK 0
#define POTRACE_TURNPOLICY_WHITE 1
#define POTRACE_TURNPOLICY_LEFT 2
#define POTRACE_TURNPOLICY_RIGHT 3
#define POTRACE_TURNPOLICY_MINORITY 4
#define POTRACE_TURNPOLICY_MAJORITY 5
#define POTRACE_TURNPOLICY_RANDOM 6

/* structure to hold tracing parameters */
struct potrace_param_s
{
  int turdsize; /* area of largest path to be ignored */
  int turnpolicy; /* resolves ambiguous turns in path decomposition */
  double alphamax; /* corner threshold */
  int opticurve; /* use curve optimization? */
  double opttolerance; /* curve optimization tolerance */
  potrace_progress_t progress; /* progress callback function */
};
typedef struct potrace_param_s potrace_param_t;

// Create default parameters
potrace_param_t *
potrace_param_default(void);

// Free parameters
void
potrace_param_free(potrace_param_t * p);



// ============================================================
//                        Curves paths
// ============================================================

/* point */
struct potrace_dpoint_s
{
  double x, y;
};
typedef struct potrace_dpoint_s potrace_dpoint_t;

/* segment tags */
#define POTRACE_CURVETO 1
#define POTRACE_CORNER 2

/* closed curve segment */
struct potrace_curve_s
{
  int n; /* number of segments */
  int * tag; /* tag[n]: POTRACE_CURVETO or POTRACE_CORNER */
  potrace_dpoint_t (*c)[3]; /* c[n][3]: control points.
                   c[n][0] is unused for tag[n]=POTRACE_CORNER */
};
typedef struct potrace_curve_s potrace_curve_t;

/* Linked list of signed curve segments. Also carries a tree structure. */
struct potrace_path_s
{
  int area; /* area of the bitmap path */
  int sign; /* '+' or '-', depending on orientation */
  potrace_curve_t curve; /* this path's vector data */

  struct potrace_path_s * next; /* linked list structure */

  struct potrace_path_s * childlist; /* tree structure */
  struct potrace_path_s * sibling; /* tree structure */

  struct potrace_privpath_s * priv; /* private state */
};
typedef struct potrace_path_s potrace_path_t;


// ======================================================================
// SHAYAN's API
// ======================================================================

//
// Transition types at a polygon midpoint
//
#define POTRACE_TRANSITION_GUESS 0
#define POTRACE_TRANSITION_SMOOTH 1
#define POTRACE_TRANSITION_CORNER 2

//
// Create or destroy a path object
//
potrace_path_t *
potrace_path_create();

void
potrace_path_free(potrace_path_t *);

//
// Setting the voxel path
//

// Set the path manually
void
potrace_path_manual(potrace_path_t *, const int n_points, const int * positions);

// Read a pathlist from a bitmap
potrace_path_t *
potrace_pathlist_automatic(potrace_bitmap_t*, potrace_param_t*); // TODO

//
// Tracing a polygon from the voxel path
//

void
potrace_ipolygon_manual(potrace_path_t *, const int n_points, const int * positions);
void
potrace_ipolygon_automatic(potrace_path_t *, potrace_param_t *);

//
// Adjusting the vertices of the polygon
//
void
potrace_dpolygon_naive(potrace_path_t *);
void
potrace_dpolygon_manual(potrace_path_t *, const int n_points, const double * positions);
void
potrace_dpolygon_automatic(potrace_path_t *, potrace_param_t *);

//
// Smooth fit to polygon using Bezier curves
//
void
potrace_smooth(potrace_path_t *, potrace_param_t *, const int n_points, const int * transition_types);

//
// This is only a view, the memory would be lost after the path is freed.
//
void
potrace_get_curve(potrace_path_t * ctx, potrace_curve_t *);
void
potrace_get_ipolygon(potrace_path_t * ctx, int *, int **);
void
potrace_get_dpolygon(potrace_path_t * ctx, int *, double **);


// ======================================================================
//       Potrace's own tracing API, which does not expose details 
// ======================================================================

// State

#define POTRACE_STATUS_OK 0
#define POTRACE_STATUS_INCOMPLETE 1

struct potrace_state_s
{
  int status;
  potrace_path_t * plist; /* vector data */

  struct potrace_privstate_s * priv; /* private state */
};
typedef struct potrace_state_s potrace_state_t;

// API FUNCTIONS

/* trace a bitmap */
potrace_state_t *
potrace_trace_bitmap(const potrace_param_t * param, const potrace_bitmap_t * bm);

/* free a Potrace state */
void
potrace_state_free(potrace_state_t * st);

/* return a static plain text version string identifying this version
   of potracelib */
const char *
potrace_version(void);


#ifdef __cplusplus
EXTERN_C_END
#endif

// ======================================================================
// UNIQUE POINTERS FOR C++
// ======================================================================

#ifdef __cplusplus
#include <memory>
namespace potrace
{
#define DEFINE_POTRACE_UNIQUE_PTR(CPP_TYPE_NAME, POTRACE_TYPE_NAME, DELETER_FUNC)                \
  class CPP_TYPE_NAME : public std::unique_ptr<POTRACE_TYPE_NAME, void (*)(POTRACE_TYPE_NAME *)> \
  {                                                                                              \
  public:                                                                                        \
    CPP_TYPE_NAME(POTRACE_TYPE_NAME * in = nullptr)                                              \
    : std::unique_ptr<POTRACE_TYPE_NAME, void (*)(POTRACE_TYPE_NAME *)>(in, DELETER_FUNC)        \
    {                                                                                            \
    }                                                                                            \
  }

DEFINE_POTRACE_UNIQUE_PTR(Param_unique_ptr, potrace_param_t, potrace_param_free);
DEFINE_POTRACE_UNIQUE_PTR(Path_unique_ptr, potrace_path_t, potrace_path_free);
DEFINE_POTRACE_UNIQUE_PTR(State_unique_ptr, potrace_state_t, potrace_state_free);
DEFINE_POTRACE_UNIQUE_PTR(Bitmap_unique_ptr, potrace_bitmap_t, potrace_bitmap_free);

#undef DEFINE_POTRACE_UNIQUE_PTR

} // end of namespace potrace
#endif //  __cplusplus


#endif /* POTRACELIB_H */
