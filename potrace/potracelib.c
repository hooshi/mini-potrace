/* Copyright (C) 2001-2017 Peter Selinger.
   This file is part of Potrace. It is free software and it is covered
   by the GNU General Public License. See the file COPYING for details. */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "curve.h"
#include "decompose.h"
#include "potracelib.h"
#include "progress.h"
#include "trace.h"

/* default parameters */
static const potrace_param_t param_default = {
    2, /* turdsize */
    POTRACE_TURNPOLICY_MINORITY, /* turnpolicy */
    1.0, /* alphamax */
    1, /* opticurve */
    0.2, /* opttolerance */
    {
        NULL, /* callback function */
        NULL, /* callback data */
        0.0,
        1.0, /* progress range */
        0.0, /* granularity */
    },
};

/* Return a fresh copy of the set of default parameters, or NULL on
   failure with errno set. */
potrace_param_t *
potrace_param_default(void)
{
  potrace_param_t * p;

  p = (potrace_param_t *)malloc(sizeof(potrace_param_t));

  if(!p)
  {
    return NULL;
  }

  memcpy(p, &param_default, sizeof(potrace_param_t));
  return p;
}

/* On success, returns a Potrace state st with st->status ==
   POTRACE_STATUS_OK. On failure, returns NULL if no Potrace state
   could be created (with errno set), or returns an incomplete Potrace
   state (with st->status == POTRACE_STATUS_INCOMPLETE, and with errno
   set). Complete or incomplete Potrace state can be freed with
   potrace_state_free(). */
potrace_state_t *
potrace_trace_bitmap(const potrace_param_t * param, const potrace_bitmap_t * bm)
{
  int r;
  path_t * plist = NULL;
  potrace_state_t * st;
  progress_t prog;
  progress_t subprog;

  /* prepare private progress bar state */
  prog.callback = param->progress.callback;
  prog.data = param->progress.data;
  prog.min = param->progress.min;
  prog.max = param->progress.max;
  prog.epsilon = param->progress.epsilon;
  prog.d_prev = param->progress.min;

  /* allocate state object */
  st = (potrace_state_t *)malloc(sizeof(potrace_state_t));

  if(!st)
  {
    return NULL;
  }

  progress_subrange_start(0.0, 0.1, &prog, &subprog);

  /* process the image */
  r = bm_to_pathlist(bm, &plist, param, &subprog);

  if(r)
  {
    free(st);
    return NULL;
  }

  st->status = POTRACE_STATUS_OK;
  st->plist = plist;
  st->priv = NULL; /* private state currently unused */

  progress_subrange_end(&prog, &subprog);

  progress_subrange_start(0.1, 1.0, &prog, &subprog);

  /* partial success. */
  r = process_path(plist, param, &subprog);

  if(r)
  {
    st->status = POTRACE_STATUS_INCOMPLETE;
  }

  progress_subrange_end(&prog, &subprog);

  return st;
}

/* free a Potrace state, without disturbing errno. */
void
potrace_state_free(potrace_state_t * st)
{
  pathlist_free(st->plist);
  free(st);
}

/* free a parameter list, without disturbing errno. */
void
potrace_param_free(potrace_param_t * p)
{
  free(p);
}

const char *
potrace_version(void)
{
#ifndef VERSION
#define VERSION ""
#endif
  return "potracelib " VERSION "";
}

// ========================================================
// Added by shayan
// ========================================================

#include "auxiliary.h"
#include <assert.h>
#include <math.h>

potrace_path_t *
potrace_path_create()
{
  potrace_path_t * ctx = path_new();
  return ctx;
}

void
potrace_path_free(potrace_path_t * ctx)
{
  pathlist_free(ctx);
}

void
potrace_path_manual(potrace_path_t * ctx, const int n_points, const int * positions)
{
  int i, len, size;
  int area;
  point_t * pt;

  len = size = 0;
  pt = NULL;
  area = 0;

  for(i = 0; i < n_points; ++i)
  {
    /* move to the next point */

    int ip1 = (i + 1) % n_points;
    int x = positions[2 * i + 0];
    int y = positions[2 * i + 1];
    // int dirx = positions[2*ip1+0]-x;
    int diry = positions[2 * ip1 + 1] - y;

    /* add point to path */
    if(len >= size)
    {
      point_t * pt1;
      size += 100;
      size = (int)(1.3 * size);
      pt1 = (point_t *)realloc(pt, size * sizeof(point_t));
      pt = pt1;
    }

    pt[len].x = x;
    pt[len].y = y;
    ++len;

    /* update area */
    area += x * diry;
  }

  ctx->priv->pt = pt;
  ctx->priv->len = len;
  ctx->area = area;
  ctx->sign = '+';
}

void
potrace_ipolygon_manual(potrace_path_t * ctx, const int n_points, const int * positions)
{

  int i;

  // Either way, these have to be calculated
  calc_sums(ctx->priv);
  calc_lon(ctx->priv);

  // length of the shortest polygon
  ctx->priv->m = n_points;

  // Set the polygon
  ctx->priv->po = (int *)calloc(ctx->priv->m, sizeof(int));

  for(i = 0; i < n_points; ++i)
  {
    ctx->priv->po[i] = positions[i];
  }
}

void
potrace_ipolygon_automatic(potrace_path_t * ctx, potrace_param_t * param)
{
  calc_sums(ctx->priv);
  calc_lon(ctx->priv);
  bestpolygon(ctx->priv);
}


void
potrace_dpolygon_naive(potrace_path_t * ctx)
{
  int i;
  privcurve_init(&ctx->priv->curve, ctx->priv->m);

  for(i = 0; i < ctx->priv->m; ++i)
  {
    ctx->priv->curve.vertex[i].x = ctx->priv->pt[ctx->priv->po[i]].x;
    ctx->priv->curve.vertex[i].y = ctx->priv->pt[ctx->priv->po[i]].y;
  }
}

void
potrace_dpolygon_manual(potrace_path_t * ctx, const int n_points, const double * positions)
{
  int i;
  privcurve_init(&ctx->priv->curve, ctx->priv->m);

  for(i = 0; i < n_points; ++i)
  {
    ctx->priv->curve.vertex[i].x = positions[2 * i + 0];
    ctx->priv->curve.vertex[i].y = positions[2 * i + 1];
  }
}

void
potrace_dpolygon_automatic(potrace_path_t * ctx, potrace_param_t * params)
{
  adjust_vertices(ctx->priv);
}

void
potrace_smooth(potrace_path_t * ctx, potrace_param_t * params, const int n_points, const int * transition_types)
{
  smooth(&ctx->priv->curve, params->alphamax, transition_types);
  if(params->opticurve)
  {
    opticurve(ctx->priv, params->opttolerance);
    ctx->priv->fcurve = &ctx->priv->ocurve;
  }
  else
  {
    ctx->priv->fcurve = &ctx->priv->curve;
  }
  privcurve_to_curve(ctx->priv->fcurve, &ctx->curve);
}


void
potrace_get_curve(potrace_path_t * ctx, potrace_curve_t * curve)
{
  *curve = ctx->curve;
}

void
potrace_get_ipolygon(potrace_path_t * ctx, int * n_po, int ** po)
{
  *n_po = ctx->priv->m;
  *po = ctx->priv->po;
}

void
potrace_get_dpolygon(potrace_path_t * ctx, int * n_po, double ** po)
{
  *n_po = ctx->priv->m;
  *po = (double *)(&ctx->priv->curve.vertex);
}
