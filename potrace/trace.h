/* Copyright (C) 2001-2017 Peter Selinger.
   This file is part of Potrace. It is free software and it is covered
   by the GNU General Public License. See the file COPYING for details. */


#ifndef TRACE_H
#define TRACE_H

#include "curve.h"
#include "potracelib.h"
#include "progress.h"

/* return a direction that is 90 degrees counterclockwise from p2-p0,
   but then restricted to one of the major wind directions (n, nw, w, etc) */
point_t
dorth_infty(dpoint_t p0, dpoint_t p2);

/* return (p1-p0)x(p2-p0), the area of the parallelogram */
double
dpara(dpoint_t p0, dpoint_t p1, dpoint_t p2);

/* ddenom/dpara have the property that the square of radius 1 centered
   at p1 intersects the line p0p2 iff |dpara(p0,p1,p2)| <= ddenom(p0,p2) */
double
ddenom(dpoint_t p0, dpoint_t p2);

/* optimize the path p, replacing sequences of Bezier segments by a
   single segment when possible. Return 0 on success, 1 with errno set
   on failure. */
int
opticurve(privpath_t * pp, double opttolerance);

/* optimize the path p, replacing sequences of Bezier segments by a
   single segment when possible. Return 0 on success, 1 with errno set
   on failure. */
void
smooth(privcurve_t * curve, double alphamax, const int * transition_types);
int
calc_lon(privpath_t * pp);
int
bestpolygon(privpath_t * pp);
int
calc_sums(privpath_t * pp);

int
adjust_vertices(privpath_t * pp);

int
process_path(path_t * plist, const potrace_param_t * param, progress_t * progress);

#endif /* TRACE_H */
