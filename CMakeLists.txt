cmake_minimum_required(VERSION 3.5)
project(potrace)

## ============================================================
## Set C++ standard and import some macros
## ============================================================

set(POTRACE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(POTRACE_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR})
include("${POTRACE_SOURCE_DIR}/cmake/potrace.cmake")
include("${POTRACE_SOURCE_DIR}/cmake/pseudo_autoconf.cmake")

## ============================================================
## CREATE CONFIG.h
## ============================================================

set(POTRACE_VERSION 3.15)
set(POTRACE_NAME "potrace")
set(MKBITMAP_NAME "mkbitmap")
# ideally set these via cmake options
set(USE_A4 0)
set(USE_METRIC 0)
set(HAVE_ZLIB 0)
set(DUMB_TTY 1)
set(HAVE_I386 0)
set(USE_LOCAL_GETOPT 1)

# unset 0 things so that config.h does not define them
macro(unset_if_zero VAR)
  if(NOT ${VAR})
    unset( ${VAR} )
  endif()
endmacro()
unset_if_zero (USE_A4)
unset_if_zero (USE_METRIC)
unset_if_zero (DUMB_TTY)
unset_if_zero (HAVE_ZLIB)
unset_if_zero (HAVE_I386)
unset_if_zero (USE_LOCAL_GETOPT)
unset(unset_if_zero)
  
add_definitions("-DHAVE_CONFIG_H=1")
configure_file("${POTRACE_SOURCE_DIR}/cmake/config.h.in" "${POTRACE_BUILD_DIR}/autogen/config.h")

## =============================================
##  Find libraries that are shared among all projects
## =============================================


# ================= Include directories

include_directories("${POTRACE_BUILD_DIR}/autogen")
#if (USE_LOCAL_GETOPT)
#  include_directories("src/include/getopt")
#endif()

# ================= Setup a target to copy all the dll files we need
# potrace_set_required_dlls()

# ================= Add subprojects
# add_subdirectory

# ================= Add targets


##
set( lib_SOURCES
  potrace/curve.c
  potrace/curve.h
  potrace/trace.c
  potrace/trace.h
  potrace/decompose.c		
  potrace/decompose.h
  potrace/potracelib.c
  potrace/potracelib.h
  potrace/lists.h
  potrace/auxiliary.h
  potrace/bitmap.h	
  potrace/progress.h
  )


##
set( minitrace_SOURCES
  potrace/potracelib_demo.c
  )

# POTRACE CORE
set(potracecore_DEPENDENCIES
  m )
add_library( potracecore ${lib_SOURCES} )


# PYPOTRACE
configure_file("${POTRACE_SOURCE_DIR}/pypotrace/setup.py.in" "${POTRACE_BUILD_DIR}/pypotrace/setup.py")

